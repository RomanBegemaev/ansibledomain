import subprocess
import yaml
import os

# Запрос выбора конфигурации сервера
server_configuration = input("Choose server configuration (1 - No 443, 2 - With 443): ")
if server_configuration == '1':
    nginx_config_file = "nginxBaseConfig.j2"
elif server_configuration == '2':
    nginx_config_file = "nginxBaseConfigSSL.j2"
else:
    print("Invalid server configuration choice. Please choose 1 or 2.")
    exit(1)

# Запрос ввода ENV 
envConf = input("enter ENV with _ : ")

server_configuration = input("Clear sites-enabled? (1 - No, 2 - Yes): ")
if server_configuration == '1':
    print("Ok!")
elif server_configuration == '2':
    yesman = 1
else:
    print("Invalid server configuration choice. Please choose 1 or 2.")
    exit(1)

# Запрос ввода IP адресов серверов
ip_addresses = input("Enter server IP addresses: ")
ip_list = [ip.strip() for ip in ip_addresses.split()]

# Запрос ввода доменных имен серверов
domains = input("Enter server domain names: ")
domain_list = [domain.strip() for domain in domains.split()]

inventory = {
    'all': {
        'hosts': {}
    }
}

# Проверка, что количество IP адресов и доменных имен совпадает
if len(ip_list) != len(domain_list):
    for ip in ip_list:
        inventory['all']['hosts'][ip] = {
            'ansible_host': ip,
            'domain': domain_list
    }
        print("The number of IPs and domains is different, put all domains on one IPs")
        command = f'ansible-playbook -i {"dynamicInventory.yaml"} {"./ansibledomain/nginxConfOneDomain.yml"} --extra-vars \'{extra_vars_yaml}\''
else:
    # Добавление IP-адресов и доменных имен в инвентарь
    for i in range(len(ip_list)):
        inventory['all']['hosts'][ip_list[i]] = {
            'ansible_host': ip_list[i],
            'domain': domain_list[i]
    }
    command = f'ansible-playbook -i {"dynamicInventory.yaml"} {"./ansibledomain/nginxConf.yml"} --extra-vars \'{extra_vars_yaml}\''

scriptDir = os.path.dirname(os.path.abspath(__file__))
file_path = os.path.join(scriptDir, 'dynamicInventory.yaml')

# Запись инвентаря в файл
with open(file_path, 'w') as file:
    yaml.dump(inventory, file, default_flow_style=False)

print("Dynamic inventory has been created successfully.")

extra_vars = {
    "server_configuration": nginx_config_file,
    "env_configuration": envConf,
}

extra_vars_yaml = yaml.dump(extra_vars)

# Команда для запуска плейбука с указанием инвентаря и дополнительных переменных
commandClear = f'ansible-playbook -i {"./ansibledomain/clearNginx.yml"} '
# Запуск команды субпроцессом
if yesman = 1:
    process = subprocess.Popen(commandClear, shell=True)
    print("Playbook has been successfully.")
process = subprocess.Popen(command, shell=True)
process.wait()